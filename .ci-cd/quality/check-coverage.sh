#!/bin/bash

echo "$COVERAGE"

if [[ "$COVERAGE" -lt "$MIN_COVERAGE" ]];
then
    echo "ERROR: A cobertura é menor do que $MIN_COVERAGE"
    exit 1
fi